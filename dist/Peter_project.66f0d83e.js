// modules are defined as an array
// [ module function, map of requires ]
//
// map of requires is short require name -> numeric require
//
// anything defined in a previous bundle is accessed via the
// orig method which is the require for previous bundles

// eslint-disable-next-line no-global-assign
parcelRequire = (function (modules, cache, entry, globalName) {
  // Save the require from previous bundle to this closure if any
  var previousRequire = typeof parcelRequire === 'function' && parcelRequire;
  var nodeRequire = typeof require === 'function' && require;

  function newRequire(name, jumped) {
    if (!cache[name]) {
      if (!modules[name]) {
        // if we cannot find the module within our internal map or
        // cache jump to the current global require ie. the last bundle
        // that was added to the page.
        var currentRequire = typeof parcelRequire === 'function' && parcelRequire;
        if (!jumped && currentRequire) {
          return currentRequire(name, true);
        }

        // If there are other bundles on this page the require from the
        // previous one is saved to 'previousRequire'. Repeat this as
        // many times as there are bundles until the module is found or
        // we exhaust the require chain.
        if (previousRequire) {
          return previousRequire(name, true);
        }

        // Try the node require function if it exists.
        if (nodeRequire && typeof name === 'string') {
          return nodeRequire(name);
        }

        var err = new Error('Cannot find module \'' + name + '\'');
        err.code = 'MODULE_NOT_FOUND';
        throw err;
      }

      localRequire.resolve = resolve;
      localRequire.cache = {};

      var module = cache[name] = new newRequire.Module(name);

      modules[name][0].call(module.exports, localRequire, module, module.exports, this);
    }

    return cache[name].exports;

    function localRequire(x){
      return newRequire(localRequire.resolve(x));
    }

    function resolve(x){
      return modules[name][1][x] || x;
    }
  }

  function Module(moduleName) {
    this.id = moduleName;
    this.bundle = newRequire;
    this.exports = {};
  }

  newRequire.isParcelRequire = true;
  newRequire.Module = Module;
  newRequire.modules = modules;
  newRequire.cache = cache;
  newRequire.parent = previousRequire;
  newRequire.register = function (id, exports) {
    modules[id] = [function (require, module) {
      module.exports = exports;
    }, {}];
  };

  for (var i = 0; i < entry.length; i++) {
    newRequire(entry[i]);
  }

  if (entry.length) {
    // Expose entry point to Node, AMD or browser globals
    // Based on https://github.com/ForbesLindesay/umd/blob/master/template.js
    var mainExports = newRequire(entry[entry.length - 1]);

    // CommonJS
    if (typeof exports === "object" && typeof module !== "undefined") {
      module.exports = mainExports;

    // RequireJS
    } else if (typeof define === "function" && define.amd) {
     define(function () {
       return mainExports;
     });

    // <script>
    } else if (globalName) {
      this[globalName] = mainExports;
    }
  }

  // Override the current require with this new one
  return newRequire;
})({"Peter_project.js":[function(require,module,exports) {
var question1 = document.getElementById('question1');
var question2 = document.getElementById('question2');
var question3 = document.getElementById('question3');
var question4 = document.getElementById('question4');
var question5 = document.getElementById('question5');
var question6 = document.getElementById('question6');
var question7 = document.getElementById('question7');
var question8 = document.getElementById('question8');
var question9 = document.getElementById('question9');
var question10 = document.getElementById('question10');
var validation = document.getElementById('validation');
var fleche1 = document.getElementById('question1__fleche1');
var fleche2 = document.getElementById('question2_phone__fleche2');
var fleche3 = document.getElementById('question3_phone__fleche3');
var fleche4 = document.getElementById('question4_phone__fleche4');
var fleche5 = document.getElementById('question5_phone__fleche5');
var fleche6 = document.getElementById('question6_phone__fleche6');
var fleche7 = document.getElementById('question7_phone__fleche7');
var fleche8 = document.getElementById('question8_phone__fleche8');
var fleche9 = document.getElementById('question9_phone__fleche9');
var fleche10 = document.getElementById('question10_phone__fleche10');
var question2_phone = document.getElementById('question2_phone');
var question3_phone = document.getElementById('question3_phone');
var question4_phone = document.getElementById('question4_phone');
var question5_phone = document.getElementById('question5_phone');
var question6_phone = document.getElementById('question6_phone');
var question7_phone = document.getElementById('question7_phone');
var question8_phone = document.getElementById('question8_phone');
var question9_phone = document.getElementById('question9_phone');
var question10_phone = document.getElementById('question10_phone');
var fin = document.getElementById('fin');
var resultat = document.getElementById('resultat');
var check1 = document.getElementById('question1__check1');
var check1_2 = document.getElementById('question1__check1_2');
var check1_3 = document.getElementById('question1__check1_3');
var check2 = document.getElementById('question2__check2');
var check2_2 = document.getElementById('question2__check2_2');
var check2_3 = document.getElementById('question2__check2_3');
var check3 = document.getElementById('question3__check3');
var check3_2 = document.getElementById('question3__check3_2');
var check3_3 = document.getElementById('question3__check3_3');
var check4 = document.getElementById('question4__check4');
var check4_2 = document.getElementById('question4__check4_2');
var check4_3 = document.getElementById('question4__check4_3');
var check5 = document.getElementById('question5__check5');
var check5_2 = document.getElementById('question5__check5_2');
var check5_3 = document.getElementById('question5__check5_3');
var check6 = document.getElementById('question6__check6');
var check6_2 = document.getElementById('question6__check6_2');
var check6_3 = document.getElementById('question6__check6_3');
var check7 = document.getElementById('question7__check7');
var check7_2 = document.getElementById('question7__check7_2');
var check7_3 = document.getElementById('question7__check7_3');
var check8 = document.getElementById('question8__check8');
var check8_2 = document.getElementById('question8__check8_2');
var check8_3 = document.getElementById('question8__check8_3');
var check9 = document.getElementById('question9__check9');
var check9_2 = document.getElementById('question9__check9_2');
var check9_3 = document.getElementById('question9__check9_3');
var check10 = document.getElementById('question10__check10');
var check10_2 = document.getElementById('question10__check10_2');
var check10_3 = document.getElementById('question10__check10_3'); //clique sur fleche,cache la div de la question et fait apparaitre celle d'apres en la faisant rebondir

fleche1.onclick = function () {
  question1.classList.add('hidden');
  question2_phone.style.display = 'block';
  question2_phone.classList.add('bounce');
}; //clique sur fleche,cache la div de la question et fait apparaitre celle d'apres en la faisant rebondir


fleche2.onclick = function () {
  question2_phone.style.display = 'none';
  question3_phone.style.display = 'block';
  question3_phone.classList.add('bounce');
}; //clique sur fleche,cache la div de la question et fait apparaitre celle d'apres en la faisant rebondir


fleche3.onclick = function () {
  question3_phone.style.display = 'none';
  question4_phone.style.display = 'block';
  question4_phone.classList.add('bounce');
}; //clique sur fleche,cache la div de la question et fait apparaitre celle d'apres en la faisant rebondir


fleche4.onclick = function () {
  question4_phone.style.display = 'none';
  question5_phone.style.display = 'block';
  question5_phone.classList.add('bounce');
}; //clique sur fleche,cache la div de la question et fait apparaitre celle d'apres en la faisant rebondir


fleche5.onclick = function () {
  question5_phone.style.display = 'none';
  question6_phone.style.display = 'block';
  question6_phone.classList.add('bounce');
}; //clique sur fleche,cache la div de la question et fait apparaitre celle d'apres en la faisant rebondir


fleche6.onclick = function () {
  question6_phone.style.display = 'none';
  question7_phone.style.display = 'block';
  question7_phone.classList.add('bounce');
}; //clique sur fleche,cache la div de la question et fait apparaitre celle d'apres en la faisant rebondir


fleche7.onclick = function () {
  question7_phone.style.display = 'none';
  question8_phone.style.display = 'block';
  question8_phone.classList.add('bounce');
}; //clique sur fleche,cache la div de la question et fait apparaitre celle d'apres en la faisant rebondir


fleche8.onclick = function () {
  question8_phone.style.display = 'none';
  question9_phone.style.display = 'block';
  question9_phone.classList.add('bounce');
}; //clique sur fleche,cache la div de la question et fait apparaitre celle d'apres en la faisant rebondir


fleche9.onclick = function () {
  question9_phone.style.display = 'none';
  question10_phone.style.display = 'block';
  question10_phone.classList.add('bounce');
}; //clique sur fleche,cache la div de la question et fait apparaitre "Resultat" et le carré blanc de fin


fleche10.onclick = function () {
  question10_phone.style.display = 'none';
  fin.style.display = 'block';
  resultat.style.display = 'block';
};

window.onload = function () {
  question1.classList.add('bounce');
};

check1.onclick = function () {
  question2.classList.add('bounce');
  question1.classList.remove('bounce');
};

check1_2.onclick = function () {
  question2.classList.add('bounce');
  question1.classList.remove('bounce');
};

check1_3.onclick = function () {
  question2.classList.add('bounce');
  question1.classList.remove('bounce');
};

question1.onmouseover = function () {
  question1.classList.remove('bounce');
};

check2.onclick = function () {
  question3.classList.add('bounce');
  question2.classList.remove('bounce');
};

check2_2.onclick = function () {
  question3.classList.add('bounce');
  question2.classList.remove('bounce');
};

check2_3.onclick = function () {
  question3.classList.add('bounce');
  question2.classList.remove('bounce');
};

question2.onmouseover = function () {
  question2.classList.remove('bounce');
};

check3.onclick = function () {
  question4.classList.add('bounce');
  question3.classList.remove('bounce');
  window.scrollTo(50, 950);
};

check3_2.onclick = function () {
  question4.classList.add('bounce');
  question3.classList.remove('bounce');
  window.scrollTo(50, 950);
};

check3_3.onclick = function () {
  question4.classList.add('bounce');
  question3.classList.remove('bounce');
  window.scrollTo(50, 950);
};

question3.onmouseover = function () {
  question3.classList.remove('bounce');
};

question3.onmouseout = function () {
  question3.classList.remove('bounce');
};

check4.onclick = function () {
  question5.classList.add('bounce');
  question4.classList.remove('bounce');
};

check4_2.onclick = function () {
  question5.classList.add('bounce');
  question4.classList.remove('bounce');
};

check4_3.onclick = function () {
  question5.classList.add('bounce');
  question4.classList.remove('bounce');
};

question4.onmouseover = function () {
  question4.classList.remove('bounce');
};

check5.onclick = function () {
  question6.classList.add('bounce');
  question5.classList.remove('bounce');
};

check5_2.onclick = function () {
  question6.classList.add('bounce');
  question5.classList.remove('bounce');
};

check5_3.onclick = function () {
  question6.classList.add('bounce');
  question5.classList.remove('bounce');
};

question5.onmouseover = function () {
  question5.classList.remove('bounce');
};

check6.onclick = function () {
  question7.classList.add('bounce');
  question6.classList.remove('bounce');
  window.scrollTo(50, 1800);
};

check6_2.onclick = function () {
  question7.classList.add('bounce');
  question6.classList.remove('bounce');
  window.scrollTo(50, 1800);
};

check6_3.onclick = function () {
  question7.classList.add('bounce');
  question6.classList.remove('bounce');
  window.scrollTo(50, 1800);
};

question6.onmouseover = function () {
  question6.classList.remove('bounce');
};

check7.onclick = function () {
  question8.classList.add('bounce');
  question7.classList.remove('bounce');
};

check7_2.onclick = function () {
  question8.classList.add('bounce');
  question7.classList.remove('bounce');
};

check7_3.onclick = function () {
  question8.classList.add('bounce');
  question7.classList.remove('bounce');
};

question7.onmouseover = function () {
  question7.classList.remove('bounce');
};

check8.onclick = function () {
  question9.classList.add('bounce');
  question8.classList.remove('bounce');
};

check8_2.onclick = function () {
  question9.classList.add('bounce');
  question8.classList.remove('bounce');
};

check8_3.onclick = function () {
  question9.classList.add('bounce');
  question8.classList.remove('bounce');
};

question8.onmouseover = function () {
  question8.classList.remove('bounce');
};

check9.onclick = function () {
  window.scrollTo(50, 2200);
  question10.classList.add('bounce');
  question9.classList.remove('bounce');
};

check9_2.onclick = function () {
  window.scrollTo(50, 2200);
  question10.classList.add('bounce');
  question9.classList.remove('bounce');
};

check9_3.onclick = function () {
  window.scrollTo(50, 2200);
  question10.classList.add('bounce');
  question9.classList.remove('bounce');
};

question9.onmouseover = function () {
  question9.classList.remove('bounce');
};

check10.onclick = function () {
  question10.classList.remove('bounce');
  validation.classList.add('bounce');
  window.scrollTo(50, 2400);
};

check10_2.onclick = function () {
  question10.classList.remove('bounce');
  validation.classList.add('bounce');
  window.scrollTo(50, 2400);
};

check10_3.onclick = function () {
  question10.classList.remove('bounce');
  validation.classList.add('bounce');
  window.scrollTo(50, 2400);
};

question10.onmouseover = function () {
  question10.classList.remove('bounce');
};

validation.onmouseover = function () {
  validation.classList.remove('bounce');
};

validation.onmouseout = function () {
  validation.classList.add('bounce');
};
},{}],"../../../usr/local/lib/node_modules/parcel-bundler/src/builtins/hmr-runtime.js":[function(require,module,exports) {
var global = arguments[3];
var OVERLAY_ID = '__parcel__error__overlay__';
var OldModule = module.bundle.Module;

function Module(moduleName) {
  OldModule.call(this, moduleName);
  this.hot = {
    data: module.bundle.hotData,
    _acceptCallbacks: [],
    _disposeCallbacks: [],
    accept: function (fn) {
      this._acceptCallbacks.push(fn || function () {});
    },
    dispose: function (fn) {
      this._disposeCallbacks.push(fn);
    }
  };
  module.bundle.hotData = null;
}

module.bundle.Module = Module;
var parent = module.bundle.parent;

if ((!parent || !parent.isParcelRequire) && typeof WebSocket !== 'undefined') {
  var hostname = "" || location.hostname;
  var protocol = location.protocol === 'https:' ? 'wss' : 'ws';
  var ws = new WebSocket(protocol + '://' + hostname + ':' + "51019" + '/');

  ws.onmessage = function (event) {
    var data = JSON.parse(event.data);

    if (data.type === 'update') {
      console.clear();
      data.assets.forEach(function (asset) {
        hmrApply(global.parcelRequire, asset);
      });
      data.assets.forEach(function (asset) {
        if (!asset.isNew) {
          hmrAccept(global.parcelRequire, asset.id);
        }
      });
    }

    if (data.type === 'reload') {
      ws.close();

      ws.onclose = function () {
        location.reload();
      };
    }

    if (data.type === 'error-resolved') {
      console.log('[parcel] ✨ Error resolved');
      removeErrorOverlay();
    }

    if (data.type === 'error') {
      console.error('[parcel] 🚨  ' + data.error.message + '\n' + data.error.stack);
      removeErrorOverlay();
      var overlay = createErrorOverlay(data);
      document.body.appendChild(overlay);
    }
  };
}

function removeErrorOverlay() {
  var overlay = document.getElementById(OVERLAY_ID);

  if (overlay) {
    overlay.remove();
  }
}

function createErrorOverlay(data) {
  var overlay = document.createElement('div');
  overlay.id = OVERLAY_ID; // html encode message and stack trace

  var message = document.createElement('div');
  var stackTrace = document.createElement('pre');
  message.innerText = data.error.message;
  stackTrace.innerText = data.error.stack;
  overlay.innerHTML = '<div style="background: black; font-size: 16px; color: white; position: fixed; height: 100%; width: 100%; top: 0px; left: 0px; padding: 30px; opacity: 0.85; font-family: Menlo, Consolas, monospace; z-index: 9999;">' + '<span style="background: red; padding: 2px 4px; border-radius: 2px;">ERROR</span>' + '<span style="top: 2px; margin-left: 5px; position: relative;">🚨</span>' + '<div style="font-size: 18px; font-weight: bold; margin-top: 20px;">' + message.innerHTML + '</div>' + '<pre>' + stackTrace.innerHTML + '</pre>' + '</div>';
  return overlay;
}

function getParents(bundle, id) {
  var modules = bundle.modules;

  if (!modules) {
    return [];
  }

  var parents = [];
  var k, d, dep;

  for (k in modules) {
    for (d in modules[k][1]) {
      dep = modules[k][1][d];

      if (dep === id || Array.isArray(dep) && dep[dep.length - 1] === id) {
        parents.push(k);
      }
    }
  }

  if (bundle.parent) {
    parents = parents.concat(getParents(bundle.parent, id));
  }

  return parents;
}

function hmrApply(bundle, asset) {
  var modules = bundle.modules;

  if (!modules) {
    return;
  }

  if (modules[asset.id] || !bundle.parent) {
    var fn = new Function('require', 'module', 'exports', asset.generated.js);
    asset.isNew = !modules[asset.id];
    modules[asset.id] = [fn, asset.deps];
  } else if (bundle.parent) {
    hmrApply(bundle.parent, asset);
  }
}

function hmrAccept(bundle, id) {
  var modules = bundle.modules;

  if (!modules) {
    return;
  }

  if (!modules[id] && bundle.parent) {
    return hmrAccept(bundle.parent, id);
  }

  var cached = bundle.cache[id];
  bundle.hotData = {};

  if (cached) {
    cached.hot.data = bundle.hotData;
  }

  if (cached && cached.hot && cached.hot._disposeCallbacks.length) {
    cached.hot._disposeCallbacks.forEach(function (cb) {
      cb(bundle.hotData);
    });
  }

  delete bundle.cache[id];
  bundle(id);
  cached = bundle.cache[id];

  if (cached && cached.hot && cached.hot._acceptCallbacks.length) {
    cached.hot._acceptCallbacks.forEach(function (cb) {
      cb();
    });

    return true;
  }

  return getParents(global.parcelRequire, id).some(function (id) {
    return hmrAccept(global.parcelRequire, id);
  });
}
},{}]},{},["../../../usr/local/lib/node_modules/parcel-bundler/src/builtins/hmr-runtime.js","Peter_project.js"], null)
//# sourceMappingURL=/Peter_project.66f0d83e.map